package ejb;

import java.util.List;

import javax.ejb.Remote;

import model.Event;
import model.User;


public interface IExampleStatelessRemote {


	public void populate();
	public void addEvent(Event event);
	public List<Event> getEventsByCity(String city);
	public List<Event> getEvents();
	public Event getEventById(long id);
	public User getUserById(long id);
	public List<User> getUsers();
	
	
}
