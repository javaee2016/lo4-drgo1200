package ejb;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import model.Event;
import model.User;
import parser.EventsParser;

@Stateless
@LocalBean
@Remote(IExampleStatelessRemote.class)
public class ExampleStatelessBean implements IExampleStatelessRemote {
	
	@PersistenceContext(unitName="example")
	private EntityManager em;
	
	public void populate() {
		EventsParser ep = new EventsParser();
		ep.parse();
		
		for(User user : ep.getUsers())
		{
			em.persist(user);
		}
	}
	
	public void addEvent(Event event) {
		em.merge(event);
	}
	
	@SuppressWarnings("unchecked")
	public List<Event> getEventsByCity(String city) {
		List<Event> events;
		events = (List<Event>) em.createQuery("SELECT e FROM Event e WHERE lower(e.city) LIKE :cityName")
				.setParameter("cityName", (city + "%").toLowerCase()).getResultList();
		return events;
	}
	public List<Event> getEvents() {
		List<Event> events;
		events = (List<Event>) em.createQuery("SELECT e FROM Event e ORDER BY e.startTime").getResultList();
		return events;
	}
	
	public Event getEventById(long id) {
		Event event;
		event = (Event) em.createQuery("SELECT e FROM Event e WHERE e.id = :id")
				.setParameter("id", id).getSingleResult();
		return event;
	}
	public User getUserById(long id) {
		User user;
		user = (User)em.createQuery("SELECT e FROM User e WHERE e.id = :id")
				.setParameter("id", id).getSingleResult();
		return user;
	}
	
	@SuppressWarnings("unchecked")
	public List<User> getUsers() {
		List<User> users;
		users = (List<User>) em.createQuery("SELECT e FROM User e").getResultList();
		return users;
	}

}
