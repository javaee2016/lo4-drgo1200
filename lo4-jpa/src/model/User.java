package model;

import java.io.Serializable;
import java.util.Set;
import java.util.HashSet;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Pattern;


@Entity
@Table(name = "USERS")
public class User implements Serializable {


	private static final long serialVersionUID = 1L;

	private long id;
	
	private String firstName;
	private String lastName;
	
	@Pattern(regexp = "[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\."
	+ "[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@"
	+ "(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9]"
	+ "(?:[a-z0-9-]*[a-z0-9])?",
	message = "{invalid.email}")
	private String email;
	
	private Set<Organizer> organizers = new HashSet<Organizer>();
	private Set<Comment> comments = new HashSet<Comment>();
	
	public User(String firstName, String lastName, String email) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
	}
	
	public User() {
		
	}
	
	public void addOrganizer(Organizer organizer){
		this.organizers.add(organizer);
	}
	
	@Id
	@GeneratedValue
	@Column(name = "USER_ID")
	public long getId() {
		return id;
	}
	
	public void setId(long id) {
		this.id = id;
	}
	
	public String getFirstName(){
		return this.firstName;
	}
	
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	
	public String getLastName(){
		return this.lastName;
	}
	
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	
	public String getEmail(){
		return this.email;
	}
	
	public void setEmail (String email) {
		this.email = email;
	}
	
	@OneToMany(mappedBy = "user",cascade=CascadeType.ALL, fetch = FetchType.EAGER)
	public Set<Organizer> getOrganizers() {
		return this.organizers;
	}
	
	public void setOrganizers(Set<Organizer> organizers) {
		this.organizers = organizers;
	}
	
	public void addOrganizers(Organizer organizer) {
		this.organizers.add(organizer);
	}
	
    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    public Set<Comment> getComments() {
        return comments;
    }
    
    public void setComments(Set<Comment> comments) {
    	this.comments = comments;
    }
	
    public void addComment(Comment comment) {
    	comments.add(comment);
    }

}
