package model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
@Table(name = "ORGANIZERS")
public class Organizer implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private long id;
	private User user;
	private Event event;
	
	@Id
	@GeneratedValue
	@Column(name = "ORGANIZER_ID")
	public long getId() {
		return id;
	}
	
	public Organizer() {
		
	}
	
	public void setId(long id) {
		this.id = id;
	}
	
    @ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinColumn(name = "USER_ID")  
    public User getUser() {
        return user;
    }
 
    public void setUser(User user) {
        this.user = user;
    }
 
    @ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinColumn(name = "EVENT_ID")
    public Event getEvent() {
        return event;
    }
 
    public void setEvent(Event event) {
        this.event = event;
    }
}
