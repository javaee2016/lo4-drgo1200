package servlet;

import java.util.concurrent.atomic.AtomicInteger;


//threadsafecounter used to show requests done by the servlets
public class ThreadSafeCounter {

	private AtomicInteger counter = new AtomicInteger(0);
	
    public void increment() {
        counter.incrementAndGet();
    }

    public int value() {
        return counter.get();
    }
}
