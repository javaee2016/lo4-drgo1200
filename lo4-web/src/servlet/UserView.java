package servlet;



import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ejb.ExampleStatelessBean;
import model.Event;
import model.Organizer;
import model.User;

/**
 * Servlet implementation class UserView
 */
@WebServlet("/User-View")
public class UserView extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@EJB
	private ExampleStatelessBean ds;

	ThreadSafeCounter counter = new ThreadSafeCounter();

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub

		counter.increment(); //invrement counter

		//create databaseservice
		//---------------------------------DatabaseService ds = (DatabaseService)getServletContext().getAttribute("db");

		Long tmpId = Long.parseLong((String) request.getParameter("userAttr")); //get id of user to show

		User user = ds.getUserById(tmpId); //ds returns a user by id

		request.setAttribute("user", user);


		request.setAttribute("comments", user.getComments());

		List<Event> events = new ArrayList<Event>(); //list of events

		for(Organizer org : user.getOrganizers()) //finds all events where user is a organizer
		{
			events.add(org.getEvent());
		}

		request.setAttribute("events", events);

		System.out.println("User: "+user.getFirstName() + " " + user.getLastName() + " " + user.getEmail());
		System.out.println("Comments: "+user.getComments().size());
		System.out.println("Events: "+events.size());

		request.setAttribute("counter", counter.value());

		request.getRequestDispatcher("/UserView.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
