package servlet;



import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

import javax.ejb.EJB;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ejb.ExampleStatelessBean;
import model.Event;
import model.Organizer;
import model.User;



/**
 * Servlet implementation class NewEvent
 */
@WebServlet("/New-Event")
public class NewEvent extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@EJB
	private ExampleStatelessBean ds;

	ThreadSafeCounter counter = new ThreadSafeCounter();


	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		//create database service
		//---------------------------------DatabaseService ds = (DatabaseService)getServletContext().getAttribute("db");

		counter.increment();//increment counter

		request.setAttribute("counter", counter.value()); //set counter as attribute

		List<User> users = ds.getUsers(); //get all users from ds

		if(request.getParameter("isSubmitted") == null) //if form havent been submitted add all users to a attribute
		{
			request.setAttribute("users", users);
			System.out.println("Not submitted");
			request.getRequestDispatcher("/NewEvent.jsp").forward(request, response);
		}
		else //else 
		{
			String status = "";
			boolean valid = true;
			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yy/MM/dd HH:mm");
			LocalDateTime start = null;
			LocalDateTime end = null;

			String eventName,city,content;
			eventName = request.getParameter("eventName");
			city = request.getParameter("city");
			content = request.getParameter("content");

			//validate all information
			try {
				String startTmp, endTmp;
				startTmp= request.getParameter("startTime");
				endTmp= request.getParameter("endTime");

				start = LocalDateTime.parse(startTmp,formatter);
				end =LocalDateTime.parse(endTmp,formatter);

			} catch(Exception e)
			{
				valid = false;
				status += "<br>invalid dateformat";
			}

			if(eventName.trim().isEmpty())
			{
				status += "<br>no event name";
				valid = false;
			}

			if(city.trim().isEmpty())
			{
				status += "<br>no city name";
				valid=false;
			}
			if(content.trim().isEmpty())
			{
				status += "<br>no content";
				valid = false;
			}

			if(request.getParameter("newOrganizers") == null)
			{
				status += "<br>no users picked";
				valid = false;
			}



			/*System.out.print(request.getParameter("startTime"));
			System.out.print(request.getParameter("endTime"));
			System.out.print(request.getParameter("eventName"));
			System.out.print(request.getParameter("city"));
			System.out.print(request.getParameter("content"));	

			String[] userOrgs = request.getParameterValues("newOrganizers");

			for(String tmpUser : userOrgs)
			{
				System.out.println(tmpUser);
			}*/

			if(valid) //if input is valid create new event, also tells the client event have been added
			{
				Event event = new Event(eventName,city,content,start,end);

				String[] orgz = request.getParameterValues("newOrganizers");

				for(String userStr : orgz)
				{
					long tmpId = Long.parseLong(userStr);

					for(User user : users)
					{
						if(user.getId() == tmpId)
						{
							Organizer tmpOrganizer = new Organizer();
							tmpOrganizer.setUser(user);
							tmpOrganizer.setEvent(event);
							user.addOrganizer(tmpOrganizer);
							event.addOrganizers(tmpOrganizer);

						}
					}

				}

				ds.addEvent(event);

				request.setAttribute("status", "Event have been added");
				request.getRequestDispatcher("/NewEvent.jsp").forward(request, response);
			}
			else //if all inputs are not valid give the client a message (status)
			{
				request.setAttribute("status", status);
				request.getRequestDispatcher("/NewEvent.jsp").forward(request, response);
			}

		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
