<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page import="java.time.LocalDateTime"%>
<%@ page import="model.Event"%>
<%@ page import="model.Comment"%>
<%@ page import="java.time.format.DateTimeFormatter"%>
<%@ page import="java.util.Set"%>
<%@ page import="java.util.ArrayList"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>User</title>
</head>
<body>
	<a href="index.jsp">Home</a>
	<p>Requests: ${counter}</p>
	<br></br>

	<p>----------User details:</p>
	<p>First name: ${user.firstName}</p>
	<p>Last name: ${user.lastName}</p>
	<p>Email: ${user.email}</p>

	<p>----------Upcomming events</p>

	<c:forEach items="${events}" var="event">

		<p>
			<c:out value="${event.title}" />
			<c:out value="${event.city}" />
			Starts:
			<%
				Event tmpEvent = (Event) pageContext.getAttribute("event");
					LocalDateTime time = tmpEvent.getStartTime();
					DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yy/MM/dd HH:mm");
					String formattedTime = time.format(formatter);
					out.println(formattedTime);
			%>
			Ends:
			<%
				LocalDateTime endTime = tmpEvent.getEndTime();
					formattedTime = endTime.format(formatter);
					out.println(formattedTime);
			%>
		</p>

	</c:forEach>

	<p>----------Posted comments</p>

	<%
		@SuppressWarnings("unchecked")
		Set<Comment> comments = (Set<Comment>) request.getAttribute("comments");

		LocalDateTime time;
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yy/MM/dd HH:mm");
		for (Comment comment : comments) {
			time = comment.getTime();
			String formattedTime = time.format(formatter);

			out.println("<p>Event: " + comment.getEvent().getTitle() + " " + comment.getEvent().getCity()
					+ " Message: " + comment.getComment() + " Time: " + formattedTime);
		}
	%>


</body>
</html>