<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page import="java.time.LocalDateTime"%>
<%@ page import="java.time.format.DateTimeFormatter"%>
<%@ page import="model.Event"%>
<%@ page import="model.User"%>
<%@ page import="java.util.List"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Events</title>
</head>
<body>
	<a href="index.jsp">Home</a>
	<p>Requests: ${counter}</p>

	<%
		if (request.getAttribute("users") != null) {
			@SuppressWarnings("unchecked")
			List<User> users = (List<User>) request.getAttribute("users");

			out.println("<form action=\"/lo4-web/New-Event\">");
			out.println("Start time: yy/mm/dd hh:mm <input type=\"text\" name=\"startTime\"/><br></br>");
			out.println("End time yy/mm/dd hh:mm <input type=\"text\" name=\"endTime\"/><br></br>");
			out.println("Event name: <input type=\"text\" name=\"eventName\"><br></br>");
			out.println("City: <input type=\"text\" name=\"city\"><br></br>");
			out.println("Content: <input type=\"text\" name=\"content\"><br></br>");
			out.println("<input type=\"hidden\" name=\"isSubmitted\" value=\"submitted\" />");

			out.println("<p>Chose users:</p>");

			for (User user : users) {
				out.println(user.getFirstName() + " " + user.getLastName() + " " + user.getId()
						+ " <input type=\"checkbox\" name=\"newOrganizers\" value=\"" + user.getId()
						+ "\"/><br></br>");
			}
			out.println("<input type=\"submit\" value=\"Add\"/>");

		}
	%>

	<p>${status}</p>
</body>
</html>