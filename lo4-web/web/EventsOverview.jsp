<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page import="java.time.LocalDateTime"%>
<%@ page import="java.time.format.DateTimeFormatter"%>
<%@ page import="model.Event"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Events</title>
</head>
<body>
	<a href="index.jsp">Home</a>
	<a href="/lo4-web/New-Event">NewEvent</a>
	<p>Requests: ${counter}</p>
	<br></br>

	<form action="/lo4-web/Events-Overview" method="GET">
		City : <input name="city" type="text" /> <input type="submit"
			value="Search" />
	</form>

	<br></br>
	<p>----------Events</p>
	<c:forEach items="${events}" var="event">

		<a href="Event-View?eventAttr=${event.id}"> <c:out
				value="${event.title}" /> <c:out value="${event.city}" /> Starts:
			<%
 	Event tmpEvent = (Event) pageContext.getAttribute("event");
 		LocalDateTime time = tmpEvent.getStartTime();
 		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yy/MM/dd HH:mm");
 		String formattedTime = time.format(formatter);
 		out.println(formattedTime);
 %> Ends: <%
 	LocalDateTime endTime = tmpEvent.getEndTime();
 		formattedTime = endTime.format(formatter);
 		out.println(formattedTime);
 %>
		</a>
		<br></br>

	</c:forEach>

</body>
</html>